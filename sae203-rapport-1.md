---
title: SAE 2.03 - Rapport
author:
- "LABUS Steve"
- "LEIBOVICI Ezechiel"
- "FAUCHOIT Lony" 
date: 24/03/2023
...

---
geometry: margin=3cm
---

---

## Rapport intermédiaire

### Questions:

#### Question(s) 1:

- Que signifie “64-bit” dans “Debian 64-bit” ?
  **-> Cela signifie que cette distribution de Debian est conçue pour fonctionner uniquement avec des processeurs 64-bits (processeur dont la largeur des registres mémoire est de 64-bits).** [^1-1]

- Quelle est la configuration réseau utilisée par défaut ?
  **-> VirtualBox utilise par défaut une configuration de type NAT (Network Access Translation), signifiant que la machine virtuelle communiquera avec l'extérieur par la même adresse IP que l'hôte.** [^1-2] \
![Paramètres réseau de VirtualBox](./img/1-2.png)
- Quel est le nom du fichier XML contenant la configuration de votre machine ?
  **-> Ce fichier se nomme "sae203.vbox"**
- Sauriez-vous le modifier directement ce fichier pour mettre 2 processeurs à votre machine ? Faites-le. 
  **-> Dans la balise Hardware puis CPU, modifier count à 2.** \
![Fichier de configuration de la VM](./img/1-4.png)

#### Question(s) 2:

- Qu’est-ce qu’un fichier iso bootable ? 
  **-> Un fichier iso bootable est une image disque (un disque virtuel) sur lequel la machine virtuelle est capable de démarrer et pouvant rendre divers services comme tester un système d'exploitation en "Live CD" ou encore aider à la maintenance de notre machine. Dans notre cas, il a servi à installer un système d'exploitation dans notre VM.**
- Qu’est-ce que MATE ? GNOME ? 
  **-> MATE et GNOME sont des environnements de bureau pour les systèmes GNU/Linux permettant d'utiliser le système à l'aide d'une interface graphique (GUI).** [^1-3]
- Qu’est-ce qu’un serveur web ? 
  **-> Un serveur web permet d'héberger sur son système des pages web ou autres accessibles sur le réseau via le protocole ***HTTP*****.[^1-4]
- Qu’est-ce qu’un serveur ssh ?
  **-> Un serveur SSH permet d'ouvrir une session à distance sur un système via un terminal.** [^1-5]
- Qu’est-ce qu’un serveur mandataire ?
  **-> Un serveur mandataire (ou proxy) est utilisé comme intermédiaire lorsque deux hôtes souhaitent communiquer entre eux.** [^1-6] \

#### Question 3:

- Comment peux-ton savoir à quels groupes appartient l’utilisateur user ?
  **-> La commande groups renvoie les groupes auxquels appartiennent l'utilisateur courant.** [^1-7] 
Exemple: \
![Extrait du terminal](./img/3-1.png)

#### Question(s) 4:

- Quel est la version du noyau Linux utilisé par votre VM ? 
  **-> La commande `uname -v` nous renvoie la version du noyau (kernel) utilisée, dans notre cas la version `5.10.162-1 (2023-01-21)`.** [^1-8] \
  ![Version du kernel](./img/4-1.png) \ 
- À quoi servent les suppléments invités ? Donner 2 principales raisons de les installer.
  **-> Les suppléments invités permettent une meilleure intégration du système invité avec le logiciel de virtualisation ainsi que le système hôte avec par exemple un pilote d'affichage avec accélération graphique pour améliorer les performances graphiques de la VM, un glisser-déposer pour facilement transférer des fichiers entre le système hôte et l'invité, ou encore la modification de la définition d'affichage lors du redimensionnement de la fenêtre du système invité.** [^1-9]
- À quoi sert la commande mount (dans notre cas de figure et dans le cas général) ?
  **-> La commande mount sert à monter un système de fichiers se trouvant sur un périphérique dont le chemin est à spécifier, dans un dossier dont le chemin est également à spécifier. Dans notre cas elle permet de monter l'image iso des suppléments invités VirtualBox se trouvant dans le lecteur CD virtuel du système, dans le dossier ```/mnt```.** [^1-10]

[^1-1]: https://fr.wikipedia.org/wiki/Processeur_64_bits
[^1-2]: https://fr.wikipedia.org/wiki/Network_address_translation
[^1-3]: https://fr.wikipedia.org/wiki/MATE, https://fr.wikipedia.org/wiki/GNOME
[^1-4]: https://fr.wikipedia.org/wiki/Serveur_web
[^1-5]: https://fr.wikipedia.org/wiki/Secure_Shell
[^1-6]: https://fr.wikipedia.org/wiki/Proxy
[^1-7]: https://wiki.debian.org/SystemGroups
[^1-8]: https://manpages.debian.org/bullseye/manpages-fr-dev/uname.2.fr.html
[^1-9]: https://docs.oracle.com/cd/E26217_01/E35193/html/qs-guest-additions.html
[^1-10]: https://manpages.debian.org/bullseye/manpages-fr/mount.8.fr.html
