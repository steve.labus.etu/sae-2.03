# SAÉ 2.03 - Rapport - Instructions
### Auteurs: LABUS Steve (steve.labus.etu@univ-lille.fr), LEIBOVICI Ezechiel (ezechiel.leibovici.etu@univ-lille.fr), FAUCHOIT Lony (lony.fauchoit.etu@univ-lille.fr)
---

Pour compiler ce rapport il est nécessaire d'installer Pandoc[^1] ainsi que le moteur de rendu XeLATeX[^2].

Il suffit ensuite simplement d'exécuter le script build.sh.

```sh
sh build.sh
```

[^1]: https://pandoc.org/installing.html
[^2]: https://xetex.sourceforge.net