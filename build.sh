#!/bin/sh
pandoc --standalone -f markdown -t  pdf sae203-rapport-*.md -o sae203-rapport.pdf --highlight-style zenburn --toc --pdf-engine=xelatex --template=templates/eisvogel.tex
pandoc --standalone -f markdown -t html sae203-rapport-*.md -o sae203-rapport.html --highlight-style zenburn --toc --template=templates/uikit.html
