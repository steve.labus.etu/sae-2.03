
## Semaine 09
---

### Questions

   1. Qu'est-ce que le projet Debian? D'où vient le nom Debian? \
        **-> Debian est une distribution spécifique du système d'exploitation Linux disposant de nombreux paquets. Le mot Debian est la contraction des noms Debra et Ian Murdock, qui a fondé le projet.** [^1] \
    2. Il existe 3 durées de prise en charge (support) de ces versions : la durée minimale, la durée en support long terme (LTS) et la durée en support long terme étendue (ELTS). Quelle sont les durées de ces prises en charge ? \
        **-> La durée minimale de support d'une version est de 1 an après la sortie de version suivante. La durée de support en long terme (LTS) est d'au moins 5 ans. La durée de support en long terme étendue étend la durée LTS d'encore 5 ans mais il ne s'agit pas d'un projet officiel de Debian.**[^2] \
    3. Pendant combien de temps les mises à jour de sécurité seront-elles fournies ? \
        **-> Les mises à jour de sécurité sont fournies pendant toute la durée de support.** \
    4. Combien de versions au minimum sont activement maintenues par Debian ? Donnez leur nom générique (= les types de distribution). \
        **-> Il y a 4 versions courantes: "oldstable" (La version précédant la version stable), "stable" (la version stable actuelle), "testing" (la prochine version stable) et "unstable" (la version de développement instable).** [^3] \
    5. Chaque distribution majeur possède un nom de code différent. Par exemple, la version majeur actuelle (Debian 11) se nomme Bullseye. D’où viennent les noms de code données aux distributions ? \
        **-> Les noms sont basés sur les noms de personnages du film Toy Story.** \
    6. L’un des atouts de Debian fut le nombre d’architecture (≈ processeurs) officiellement prises en charge. Combien et lesquelles sont prises en charge par la version Bullseye ? \
        **-> 9 architectures sont prises en charges: amd64, i386, ppc64el, s390x, armel, armhf, arm64, mipsel et mips64el.** [^4] \
    7. Première version avec un nom de code \
        **-> Debian 1.1 "Buzz" annoncée le 17 juin 1996** [^5]\
    8. Dernière version avec un nom de code \
        **-> Debian 14 "Forky" annoncée le 13 octobre 2022** [^3]\

### Installation préconfigurée

Le fichier SAE203-Debian.viso est un fichier texte contenant des instructions à donner à VirtualBox pour le premier démarrage de la VM. Il indique le chemin des différentes images disque que l'on souhaite monter ainsi que les fichiers de configuration que l'on souhaite rendre accessibles à l'intérieur de la VM pour l'installateur de Debian. \
![Contenu du fichier SAE203-Debian.viso](./img/s09/1.png)
Le fichier preseed-fr.cfg contient les instructions lues par l'installateur de Debian afin de partitionner le disque, configurer le réseau, les utilisateurs et les paquets à installer. \
![Contenu du fichier preseed-fr.cfg](./img/s09/2.png)


En y ajoutant la ligne suivante, on peut automatiser l'installation  de sudo, git, sqlite3, curl, bash-completion et neofetch[^6]:
```d-i pkgsel/include string sudo git sqlite3 curl bash-completion neofetch```
Ajouter "mate-desktop" à la ligne suivante permet d'installer l'environnement de bureau MATE à l'installation:
```tasksel tasksel/first multiselect standard ssh-server```
Les deux dernières lignes de cette instruction permettent à l'utilisateur user d'utiliser sudo sans mot de passe[^7]:
```sh
d-i preseed/late_command string cp /cdrom/vboxpostinstall.sh /target/root/vboxpostinstall.sh \
 && chmod +x /target/root/vboxpostinstall.sh \
 && /bin/sh /target/root/vboxpostinstall.sh --need-target-bash --preseed-late-command \
 && echo 'user ALL=(ALL) NOPASSWD: ALL' > /target/etc/sudoers.d/user  \
    && in-target chmod 440 /etc/sudoers.d/user \
```

Il suffit ensuite d'ajouter le fichier SAE203-Debian.viso au lecteur virtuel de la VM et lors du démarrage l'installation sera entièrement automatisée.
\
![Paramètres de la VM](./img/s09/3.png)

[^1]:https://www.debian.org/doc/manuals/debian-faq/basic-defs.fr.html#pronunciation
[^2]:https://wiki.debian.org/fr/LTS
[^3]:https://wiki.debian.org/fr/DebianReleases
[^4]: https://wiki.debian.org/fr/DebianBullseye
[^5]: https://www.debian.org/doc/manuals/project-history/project-history.fr.pdf
[^6]: https://debian-facile.org/doc:install:preseed
[^7]: https://stackoverflow.com/questions/32272413/passwordless-sudo-using-ubuntu-preseed-and-packer
