
## Semaines 10 et 11
---

### Préliminaire

D'abord configurons notre client git puis installons le paquet git-gui, avec les commandes suivantes :

```sh
git config --global user.name "Prénom Nom" 
git config --global user.email "votre@email" 
git config --global init.defaultBranch "master"
sudo apt update && sudo apt install git-gui
```

#### Questions

- Qu'est-ce que le logiciel git-gui? Comment se lance-t-il?
  **-> Le logiciel git-gui est un client git en interface graphique. Il se lance à l'aide de la commande ```git gui```.** \
- Mêmes questions avec gitk
  **-> Le logiciel gitk est un autre client git en interface graphique qui lui se lance à l'aide de la commande ```gitk``` dans le bon répertoire de travail.** \
  ![Git Gui et gitk](./img/s10-11/1.png)
- Quelle sera la ligne de commande git pour utiliser par défaut le proxy de l’université sur tous vos projets git ?
  **-> ```git config --global http.proxy http://cache.univ-lille.fr:3128```** [^8]

Pour le service que nous souhaitons héberger nous devons exposer le port 3000 de la VM: \
   ![Redirections de port VirtualBox](./img/s10-11/2.png)

### 2. Installation de Gitea

#### Questions

- Qu'est-ce que Gitea? \
  **-> Gitea est un service d'hébergement Git libre auto-hébergeable.**[^9]
- À quels logiciels bien connus dans ce domaine peut-on le comparer (en citer au moins 2) ? \
  **-> On peut le comparer à GitLab dans sa version CE (Community Edition) gratuite et auto-hébergeable ou à GitHub bien que lui ne soit ni libre ni auto-hébergeable.**

#### 2.1. Installation

L'installation s'effectuera en se basant sur la documentation officielle[^9]. \

1. **Télécharger Gitea**:  Passons en mode sudo. Nous allons télécharger la version 1.18.5 de Gitea, pour Linux en architecture amd64, à l'aide de la commande wget suivante puis rendre le binaire exécutable avec chmod:

   ```sh
   sudo -s
   wget -O gitea https://dl.gitea.com/gitea/1.18.5/gitea-1.18.5-linux-amd64 
   chmod +x gitea
   ```

   1.1. **Vérifier la signature du binaire**: à l'aide des commandes suivantes, pour s'assurer de son authenticité, le résultat attendu est ```Good signature from Teabot```: \

   ```sh
   gpg --keyserver keys.openpgp.org --recv 7C9E68152594688862D62AF62D9AE806EC1592E2
   gpg --verify gitea-1.18.5-linux-amd64.asc gitea-1.18.5-linux-amd64
   ```

   \
2. **Préparer l'environnement**: Nous allons maintenant préparer notre système à l'installation de Gitea en créant un utilisateur chargé d'éxécuter le service ainsi que l'arborescence nécessaire pour son fonctionnement: \

   ```sh
   # Création de l'utilisateur
    adduser \
        --system \
        --shell /bin/bash \
        --gecos 'Git Version Control' \
        --group \
        --disabled-password \
        --home /home/git \
        git
   # Arborescence
    mkdir -p /var/lib/gitea/{custom,data,log}
    chown -R git:git /var/lib/gitea/
    chmod -R 750 /var/lib/gitea/
    mkdir /etc/gitea
    chown root:git /etc/gitea
    chmod 770 /etc/gitea
    export GITEA_WORK_DIR=/var/lib/gitea/
    cp gitea /usr/local/bin/gitea
   ```

   \
3. **Éxécution de Gitea en tant que service**: Cela permet au système de lancer Gitea au démarrage, nous allons mettre cela en place à l'aide des commandes suivantes, en récupérant la configuration de service par défaut: \

   ```sh
    wget -O /etc/systemd/system/gitea.service https://github.com/go-gitea/gitea/raw/main/contrib/systemd/gitea.service
    systemctl enable gitea && systemctl start gitea
   ```

   \
4. **Configuration de Gitea**: Vérifions si le service s'est bien lancé à l'aide de la commande suivante, qui devrait nous afficher "active" si c'est bien le cas: \

    ```sh
    systemctl status gitea.service
    ```

    ![Statut du service Gitea](./img/s10-11/3.png)
    Ensuite nous pouvons nous rendre sur ```localhost:3000``` à l'aide d'un navigateur pour accéder à la configuration de Gitea.
   - La base de données sera ```SQLite3```
   - Le compte administrateur web sera :
      -  Nom : gitea
      - Password : gitea
      -  Email : git@localhost

5. **Finalisation**: Une fois Gitea prêt, exécutez les commandes suivantes afin de protéger les répertoires de Gitea en écriture: \

   ```sh
   chmod 750 /etc/gitea
   chmod 640 /etc/gitea/app.ini
   ```

### 3. Mise à jour de Gitea [^10]

Un script est disponible sur le repo GitHub de Gitea afin d'automatiser la procédure pour mettre à jour notre instance de Gitea. Il s'occupe de stopper le service ainsi que de sauvegarder les données avant de procéder à la mise à jour, voici comment l'utiliser:

1. **Récupérer le script**: Téléchargez le script à l'aide de la commande suivante et installez la dépendance manquante: \

   ```sh
    wget https://github.com/go-gitea/gitea/raw/main/contrib/upgrade.sh
    apt install jq
   ```

2. **Éxécuter le script**: La commande suivante est suffisante si vous avez suivi les instructions ci-dessus. Vous pouvez également préciser en argument un numéro de version. Par défaut la dernière version sera installée:

   ```sh
   ./upgrade.sh
   ```

### 4. Utilisation de Gitea

Maintenant que l'installation est terminée il est possible de créer des dépôts depuis l'interface web de Gitea, via le bouton + de la barre de navigation et en remplissant les infos de notre dépôt. \
![Ajouter un dépôt](./img/s10-11/4.png)

Une fois le dépôt créé il est possible de consulter, créer outéléverser un fichier depuis l'interface web ainsi que de copier l'adresse HTTP ou SSH du dépôt pour push un dépôt depuis notre ordinateur. \
![Présentation d'un dépôt](./img/s10-11/5.png)

**Attention**: Pour certains projets existants, un problème peut survenir lié à la branche principale. En effet Gitea utilise par défaut le nom "main" pour définir la branche principale d'un dépôt plutôt que le traditionnel "master". Il est possible de modifier cela en se rendant dans le fichier ```/etc/gitea/app.ini``` et en ajoutant la ligne ```DEFAULT_BRANCH = master``` à cet endroit[^11]: \
![Branche master par défaut](./img/s10-11/6.png)

[^8]:https://gist.github.com/evantoli/f8c23a37eb3558ab8765
[^9]:https://docs.gitea.io/en-us/install-from-binary/
[^10]:https://docs.gitea.io/en-us/upgrade-from-gitea/
[^11]:https://stackoverflow.com/questions/73847901/gitea-how-to-change-the-default-branch-name-back-to-master
